//
//  HostingController.swift
//  ArtAround WatchKit Extension
//
//  Created by Domenico Varchetta on 17/01/2020.
//  Copyright © 2020 Domenico Varchetta. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<ContentView> {
    override var body: ContentView {
        return ContentView()
    }
}

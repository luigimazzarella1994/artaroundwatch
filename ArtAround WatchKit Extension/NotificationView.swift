//
//  NotificationView.swift
//  ArtAround WatchKit Extension
//
//  Created by Domenico Varchetta on 17/01/2020.
//  Copyright © 2020 Domenico Varchetta. All rights reserved.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}

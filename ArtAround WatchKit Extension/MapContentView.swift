//
//  MapContentView.swift
//  ArtAround WatchKit Extension
//
//  Created by Domenico Varchetta on 17/01/2020.
//  Copyright © 2020 Domenico Varchetta. All rights reserved.
//

import SwiftUI

struct MapContentView: View {
    var body: some View {
        VStack() {
            
            Text("Map")
                .font(.headline)
                .multilineTextAlignment(.center)
                .lineLimit(1)
            
        }.padding(.all, 0.0).navigationBarTitle(Text("ArtAround"))
    }
}

struct MapContentView_Previews: PreviewProvider {
    static var previews: some View {
        MapContentView()
    }
}

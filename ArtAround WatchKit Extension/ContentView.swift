//
//  ContentView.swift
//  ArtAround WatchKit Extension
//
//  Created by Domenico Varchetta on 17/01/2020.
//  Copyright © 2020 Domenico Varchetta. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @State var didTap = false
    
    var body: some View {
        VStack() {
            Image("circleLogo")
                .resizable()
                .frame(width: 37, height: 37)
            
            Text("Welcome!")
                .font(.headline)
                .multilineTextAlignment(.center)
                .lineLimit(1)
            Text("Start monitoring POIs")
                .multilineTextAlignment(.center)
                .lineLimit(2)
            
            Button(action: {
                self.didTap = !self.didTap
            }) {
                Text(didTap ? "STOP" : "START")
            }
            .background(didTap ? Color.red : Color.clear)
            .cornerRadius(7.0)
            // cornerRadius required because red color view
            // does not have rounded borders.
            // 7.0 is the same value as the default button
            
        }
        .padding(.all, 0.0)
        .navigationBarTitle(Text("ArtAround"))
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
